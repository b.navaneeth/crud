const express = require("express");
const path = require("path");
const ejs = require("ejs");

const app = express();
const { port } = require("./config");
const createData = require("./routes/create");
const showData = require("./routes/showData");
const deleteData = require("./routes/delete");
const updateData = require("./routes/update");

app.use(express.static(`${__dirname}/../public`));
app.set("views", path.join(__dirname, "../public"));
app.set("view engine", "ejs");

showData()
    .then((data) => {
        app.use(data);
        return deleteData();
    })
    .then((data) => {
        app.use("/delete", data);
        return createData();
    })
    .then((data) => {
        app.use("/create", data);
        return updateData();
    })
    .then((data) => {
        app.use(data);
    })
    .catch((err) => {
        console.log(err);
    });

app.use(function(err, req, res, next) {
    console.log(err);
    res.status(500);
    res.render("error");
});

app.listen(port);