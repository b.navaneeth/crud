const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const addData = require("../queries/addData");

let urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function() {
    return new Promise((resolve, reject) => {
        try {
            router.get("/", (req, res) => {
                res.status(200);
                res.render("create");
            });

            router.post("/", urlencodedParser, async(req, res, next) => {
                try {
                    await addData(req.body);
                    res.redirect("/");
                } catch (err) {
                    next(err);
                }
            });
            resolve(router);
        } catch (err) {
            reject(err);
        }
    });
};