const Connect = require("../connect.js");

module.exports = function(data) {
    return new Promise((resolve, reject) => {
        let userData = {
            userID: data.user_id,
            title: data.title,
            body: data.body,
        };
        let insert = `INSERT INTO users SET ?`;

        Connect.getConnection((err, connection) => {
            if (err) {
                reject(err);
            } else {
                connection.query(insert, userData, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                    connection.release();
                });
            }
        });
    });
};